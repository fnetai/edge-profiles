# Developer Guide for `@fnet/edge-profiles`

## Overview

The `@fnet/edge-profiles` library provides a straightforward way to access and enumerate user profiles from the Edge browser. This library focuses on extracting profile information such as profile names and directory paths from the browser's local data. It is useful for developers who need to programmatically interact with or manage Edge browser profiles.

## Installation

To install the `@fnet/edge-profiles` library, you can use either npm or yarn. Run one of the following commands in your project's root directory:

Using npm:
```bash
npm install @fnet/edge-profiles
```

Using yarn:
```bash
yarn add @fnet/edge-profiles
```

## Usage

Below is a guide on how to use the `@fnet/edge-profiles` library to list Edge browser user profiles. The core function provided by this library is the default export, which allows you to retrieve browser profile information.

### List Edge Browser Profiles

You can use the default function from the library to get a list of Edge browser profiles. It returns a promise that resolves to an array of objects, each containing the name and directory of a profile.

```javascript
import edgeProfiles from '@fnet/edge-profiles';

// Calling the function without arguments to use the default path
edgeProfiles()
  .then(profiles => {
    profiles.forEach(profile => {
      console.log(`Profile Name: ${profile.name}, Profile Directory: ${profile.dir}`);
    });
  })
  .catch(err => {
    console.error('Error retrieving profiles:', err.message);
  });
```

If you want to specify a custom path to the Edge user data directory, you can pass an object with an `edgeDataPath` property:

```javascript
import edgeProfiles from '@fnet/edge-profiles';

const customPath = '/path/to/custom/edge/user/data';

edgeProfiles({ edgeDataPath: customPath })
  .then(profiles => {
    profiles.forEach(profile => {
      console.log(`Profile Name: ${profile.name}, Profile Directory: ${profile.dir}`);
    });
  })
  .catch(err => {
    console.error('Error retrieving profiles:', err.message);
  });
```

## Examples

Here are some scenarios where this library can be particularly useful:

### Example 1: List Default Profiles

This example shows how to list all the user profiles stored in the default Edge user data directory.

```javascript
import edgeProfiles from '@fnet/edge-profiles';

edgeProfiles().then(profiles => {
  console.log('Edge Profiles:', profiles);
}).catch(console.error);
```

### Example 2: List Profiles from a Custom Path

Sometimes the user data may not be located in the default directory. In such cases, you can specify the path to the data.

```javascript
import edgeProfiles from '@fnet/edge-profiles';

edgeProfiles({ edgeDataPath: '/custom-path' })
  .then(profiles => {
    console.log('Custom Path Edge Profiles:', profiles);
  })
  .catch(console.error);
```

## Acknowledgement

This project appreciates the effort and contributions of developers and contributors of the libraries and tools that `@fnet/edge-profiles` depends on. Their work has been instrumental in making this library functional and reliable.