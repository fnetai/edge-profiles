# @fnet/edge-profiles

## Introduction

The `@fnet/edge-profiles` project is a simple utility designed to help users easily retrieve the names of user profiles from the Microsoft Edge browser. By accessing and reading the 'Local State' file within Edge's user data directory, this utility simplifies the task of identifying different user profiles set up in the browser. This can be particularly useful for users who need to manage or differentiate between multiple profiles on a single system.

## How It Works

The tool operates by locating the Edge browser's user data directory either through a specified path or by using the default path based on the operating system. It then reads the 'Local State' file found within this directory and extracts the names of the profiles listed within the browser. The utility returns these profile names, providing an insight into the various user profiles configured in Edge.

## Key Features

- **Platform-Aware:** Automatically determines the standard user data path for Edge, depending on whether you're using Windows, macOS, or Linux.
- **Profile Listing:** Reads the necessary configuration file and lists all the user profile names associated with the Edge browser installation.
- **Error Handling:** Provides feedback if it fails to read the profiles, helping users diagnose issues related to path or permission errors.

## Conclusion

`@fnet/edge-profiles` is a straightforward utility for users who need to identify and work with multiple user profiles in Microsoft Edge. By automating the process of reading and listing these profiles, it simplifies the task, making it hassle-free to manage user data configurations.